﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data.Entity;
using ToDo.Entities;


namespace ToDo.DataModel
{
  class ToDoContext : DbContext
  {
    public DbSet<TaskEntity> TaskEntities { get; set; }
  }
}