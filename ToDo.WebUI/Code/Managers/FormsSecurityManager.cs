﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.Mvc;

using ToDo.Repositories;

namespace ToDo.WebUI.Code.Managers
{
    public class FormsSecurityManager : ISecurityManager
    {
        private IUserRepository _iuserRepository;

        #region Constructors
        public FormsSecurityManager(IUserRepository iuserRepository)
        {
            this._iuserRepository = iuserRepository;
        }
        #endregion

        public bool Login(string login, string password)
        {
            //string _login = "user1";
            //string _password = "pass1";
            //return ((login == _login && password == _password) ? true : false);

            if (_iuserRepository.IsUser(login, password) )
            {
                FormsAuthentication.SetAuthCookie(login, false);
                return true;
            }
            else
            {
                return false;
            }

        }

        public void Logout()
        {
            FormsAuthentication.SignOut();
            //return RedirectToAction("Login", "Security");
        }

    }
}