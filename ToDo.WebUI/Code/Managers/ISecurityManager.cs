﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToDo.WebUI.Code.Managers
{
    public interface ISecurityManager
    {
        bool Login(string username, string password);
        void Logout();
    }
}
