﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using ToDo.Entities;
using ToDo.Repositories;
using ToDo.WebUI.Models;

namespace ToDo.WebUI.Controllers
{
    [Authorize]
    public class ToDoController : Controller
    {
        private ITaskRepository _taskRepository;

        #region Constructors
        //public ToDoController()
        //{
        //  //  reference to 1(!) static FakeRepository
        //  this._taskRepository = new FakeTaskRepository();
        //}

        public ToDoController(ITaskRepository taskRepository)
        {
            this._taskRepository = taskRepository;
        }
        #endregion

        // GET: /ToDo/
        [HttpGet]
        public ActionResult Index()
        {
            //FakeTaskRepository repository = new FakeTaskRepository();
            //ViewBag.tasks = repository.GetAll();

            ViewBag.tasks = _taskRepository.GetAll();
            return View();
        }

        // POST: /ToDo/ --- task addition
        [HttpPost]
        //public ViewResult Index(string txtTitle="")
        public ActionResult Index(string txtTitle = "")
        {
            #region Validation
            if (String.IsNullOrWhiteSpace(txtTitle) || String.IsNullOrEmpty(txtTitle))
            {
                ViewBag.Error = "ERROR : The task title can not be empty!";
                txtTitle = null;
            }
            else if (txtTitle.Length > 10)
            {
                ViewBag.Error = "ERROR : The task title can not be more 10 symbols!";
                txtTitle = null;
            }
            else if (!String.IsNullOrEmpty(txtTitle))
            {
                _taskRepository.Add(txtTitle);
                txtTitle = "";
            }
            #endregion

            ViewBag.Tasks = this._taskRepository.GetAll();

            //return View();
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult Delete(int taskId)
        {
            _taskRepository.Remove(taskId);

            ViewBag.Tasks = this._taskRepository.GetAll();
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            TaskEntity taskEntity = this._taskRepository.Get(id);

            TaskModel taskModel = new TaskModel()
            {
                Id = taskEntity.Id,
                Title = taskEntity.Title,
                IsDone = taskEntity.IsDone
            };

            //return View(taskEntity);
            return View(taskModel);
        }

        [HttpPost]  //  !bad
        public ActionResult Edit(TaskModel taskModel)
        {
            #region Save in Edit
            if (ModelState.IsValid)
            {
                TaskEntity taskEntityTemp = new TaskEntity();

                taskEntityTemp.Id = taskModel.Id;
                taskEntityTemp.Title = taskModel.Title;
                taskEntityTemp.IsDone = (bool)taskModel.IsDone;

                _taskRepository.Remove(taskModel.Id);
                _taskRepository.Add(taskEntityTemp);

                //_taskRepository.SaveChanges();
                return RedirectToAction("Index");
            }
            #endregion
            else
            {
                return View(taskModel);
            }
            return RedirectToAction("Save");
        }

        [HttpPost]  //  !bad
        public ActionResult Save(TaskModel taskModel)
        {
            if (ModelState.IsValid)
            {
                TaskEntity taskEntityTemp = new TaskEntity();

                taskEntityTemp.Id = taskModel.Id;
                taskEntityTemp.Title = taskModel.Title;
                taskEntityTemp.IsDone = (bool)taskModel.IsDone;

                this._taskRepository.Add(taskEntityTemp);

                //_taskRepository.SaveChanges();
                return RedirectToAction("Index");
            }

            #region Old Save of Edit results
            //if (ModelState.IsValid)
            //{
            //  return RedirectToAction("Index", new { id = taskModel.Id });
            //} 
            #endregion

            return View(taskModel);
        }

        [HttpPost]  //  ok
        public ActionResult Cancel()
        {
            return RedirectToAction("Index");
        }

        public ActionResult ChangeStatus(int taskId, bool isDone)
        {
            //TaskEntity taskEntity = new TaskEntity();
            //taskEntity.Id = taskId;
            //taskEntity.IsDone = isDone;

            //_taskRepository.UpdateStatusTask(taskEntity);
            //ViewBag.Tasks = this._taskRepository.GetAll();
            //return RedirectToAction("Index");

            //  execute repository
            return new EmptyResult();
        }

        [HttpPost]
        public ActionResult TasksPartial(string taskStatus)
        {
            List<TaskEntity> tasks2 = _taskRepository.GetAll().Where(m => m.IsDone).ToList();

            return PartialView("TasksPartial", tasks2);
        }
    }
}
