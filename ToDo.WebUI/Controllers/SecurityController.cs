﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using ToDo.WebUI.Models;
using ToDo.WebUI.Code.Managers;

namespace ToDo.WebUI.Controllers
{
    public class SecurityController : Controller
    {
        private ISecurityManager _securityManager;

        #region Constructors
        public SecurityController(ISecurityManager securityManager)
        {
            this._securityManager = securityManager;
        }
        #endregion

        // GET: /Security/
        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }

        // POST: /Security/
        [HttpPost]
        //public ActionResult Login(string username, string password)
        public ActionResult Login(LoginModel loginModel)
        {
            //if (ModelState.IsValid)
            if (this._securityManager.Login(loginModel.UserName, loginModel.Password))
            {
                //ViewBag.Message = string.Format("login : {0} password : {1} ",
                //    loginModel.UserName, loginModel.Password);

                //return RedirectToAction("Login", "Security");
                return RedirectToAction("Index", "ToDo");
                //return View(loginModel);
            }
            else
            {
                // there is something wrong with the data values
                // return View(loginModel);

                ViewBag.Message = "Incorrect user or password";
                return View();
            }

        }

        public ActionResult Logout()
        {
            return RedirectToAction("Login", "Security");
        }
    }
}
