﻿CREATE DATABASE ToDo_MAA;
GO

USE ToDo_MAA;
GO

CREATE TABLE TaskEntities
(
	Id int not null identity(1,1),
	Title nvarchar(50) not null,
	IsDone INT not null,
	constraint PK_TaskEntities_Id primary key (Id)
)
GO

set identity_insert TaskEntities on

INSERT INTO TaskEntities 
		(Id, Title, IsDone) 
	VALUES 
		(1, 'Task1', 1),
		(2, 'Task2', 1), 
		(3, 'Task3', 0),
		(4, 'Task4', 0),
		(5, 'Task5', 0),
		(6, 'Task6', 1)

set identity_insert TaskEntities off
GO
