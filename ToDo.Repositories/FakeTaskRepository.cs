﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ToDo.Entities;

namespace ToDo.Repositories
{
    public class FakeTaskRepository : ITaskRepository
    {
        //ITaskRepository _faketasktrepository;
        static List<TaskEntity> _faketasktrepository = null;

        static FakeTaskRepository()
        {
            _faketasktrepository = new List<TaskEntity>();

            //TaskEntity taskentity1 = new TaskEntity() {Id=1, Title="title1", IsDone=true};
            _faketasktrepository.Add(new TaskEntity() { Id = 1, Title = "title 1", IsDone = true });
            _faketasktrepository.Add(new TaskEntity() { Id = 2, Title = "title 2", IsDone = false });
            _faketasktrepository.Add(new TaskEntity() { Id = 3, Title = "title 3", IsDone = true });
        }

        //public FakeTaskRepository( ITaskRepository faketasktrepository)
        //{
        //    _faketasktrepository = faketasktrepository;
        //}

        public List<TaskEntity> GetAll()
        {
            List<TaskEntity> list = new List<TaskEntity>();

            foreach (var item in _faketasktrepository)
            {
                list.Add(item);
            }

            return list;
        }

        //public List<TaskEntity> Add(string title)
        public void Add(string title)
        {
            #region Validation
            if (String.IsNullOrWhiteSpace(title) || String.IsNullOrEmpty(title))
            {
                throw new ArgumentException("Error! The task can not be empty!");
            }
            if (title.Length > 10)
            {
                throw new ArgumentException("Error! The task can not be more 10 symbols!");
            }
            #endregion

            TaskEntity taskEntityTemp = new TaskEntity();

            int maxIndex = 0;
            foreach (TaskEntity te in _faketasktrepository)
            {
                maxIndex = (te.Id > maxIndex) ? te.Id : maxIndex;
            }

            taskEntityTemp.Id = maxIndex + 1;
            //Count + 1;
            taskEntityTemp.Title = title;
            taskEntityTemp.IsDone = false;

            _faketasktrepository.Add(taskEntityTemp);
            taskEntityTemp = null;
            title = "";

            //return _faketasktrepository;
        }

        //public List<TaskEntity> Add(TaskEntity taskEntityTemp)
        public void Add(TaskEntity taskEntityTemp)
        {
            _faketasktrepository.Add(taskEntityTemp);

            //return _faketasktrepository;
        }

        public void Remove(int id)
        {
            TaskEntity taskEntity = null;

            taskEntity = _faketasktrepository.Find(r => r.Id == id);
            _faketasktrepository.Remove(taskEntity);
            //_faketasktrepository.RemoveAll(r => r.Id == id);
        }

        public TaskEntity Get(int id)
        {
            //TaskEntity taskentity = _faketasktrepository[id];

            foreach (TaskEntity taskentity in _faketasktrepository)
            {
                if (taskentity.Id == id)
                {
                    return taskentity;
                }
            }
            throw new ArithmeticException("ERROR : TaskEntity wrong Id");
        }

        // not done !
        public void UpdateStatusTask(TaskEntity taskEntity)
        {
            TaskEntity taskEntityTemp = null;
            taskEntityTemp = _faketasktrepository.Find(r => r.Id == taskEntity.Id);

            _faketasktrepository.Remove(taskEntityTemp);
            _faketasktrepository.Add(taskEntity);
        }

    }
}
