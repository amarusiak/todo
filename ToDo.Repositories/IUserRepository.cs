﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToDo.Repositories
{
    public interface IUserRepository
    {
        bool IsUser(string login, string password);
    }
}
