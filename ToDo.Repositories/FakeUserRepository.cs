﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ToDo.Entities;

namespace ToDo.Repositories
{
    public class FakeUserRepository : IUserRepository
    {
        static List<UserEntity> _fakeuserrepository = null;

        static FakeUserRepository()
        {
            _fakeuserrepository = new List<UserEntity>();

            _fakeuserrepository.Add(new UserEntity() { Login = "user1", Password = "pass1" });
            _fakeuserrepository.Add(new UserEntity() { Login = "user2", Password = "pass2" });
        }
        public bool IsUser(string login, string password)
        {
            foreach (UserEntity userentity in _fakeuserrepository )
            {
                if ( (userentity.Login == login) && (userentity.Password == password) )
                {
                    return true;
                }
            }
            //throw new ArithmeticException("ERROR : UserEntity wrong login or password");
            return false;
        }
    }
}
