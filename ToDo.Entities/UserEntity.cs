﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToDo.Entities
{
    public class UserEntity
    {
        public string Login { get; set; }
        public string Password { get; set; }
    }
}
